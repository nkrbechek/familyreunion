import Route from '@ember/routing/route';
import { hash } from 'rsvp';


export default Route.extend({
  model() {
   return hash({
     family: this.store.findAll('family'),
     individual: this.store.findAll('individual'),
     admin: this.store.findAll('admin'),
   });
 },

 setupController(controller, models) {
   controller.set('admin', models.admin.get('firstObject'));
   controller.set('family', models.family);
   controller.set('individual', models.individual);
   // or, more concisely:
   // controller.setProperties(models);
 }
});
