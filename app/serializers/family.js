import FirebaseSerializer from 'emberfire/serializers/firebase';

export default FirebaseSerializer.extend({
  attrs: {
   contact: { embedded: 'always' },
   children: { embedded: 'always' },
 }
});
