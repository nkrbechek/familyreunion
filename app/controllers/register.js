import Controller from '@ember/controller';
import { A } from '@ember/array';

export default Controller.extend({
  relatives: A([
    "John V.",
    "Donna",
    "Swen",
    "Ray",
    "Mable",
    "Verl",
    "Anna",
    "Nellie",
  ]),
  relative: "",
  form: "",
  formType: '',
  nextButton: "family",
  complete: false,
  actions: {
     selectPerson(value) {
       this.set('relative', value); // should set
     },
     selectForm(type) {
       let addr = this.store.createRecord('address', {})
       let contact = this.store.createRecord('contact', {
         address: addr
       })
       let form = this.store.createRecord(type, {
         relative: this.get('relative'),
         contact: contact
       })
       this.set('form', form)
       this.set('formType', type)
       if (type === "individual") {
         this.set('nextButton', "family")
       }
     },
   }
});
