import Controller from '@ember/controller';
import sha1 from 'sha1';

export default Controller.extend({
  test: sha1("test"),
  password: "",
  username: "",
  signedIn: false,
  signInErr: false,
  totalAdults: 0,
  totalBabies: 0,
  totalKids: 0,
  totalTeens: 0,
  totalPreTeen: 0,
  actions: {
   signIn() {
     if (this.get('admin.password') === sha1(this.get('password')) && this.get('admin.username') === this.get('username')) {
        var family = this.get("family")
        var tA = 0
        var tB = 0
        var tK = 0
        var tT = 0
        var tP = 0
        family.forEach(function(item){
          tA += item.get('numAdults')
          tB += item.get('numberBabies')
          tK += item.get('numberChildren')
          tT += item.get('numberTeens')
          item.get('children').forEach(function(kid){
            if (kid.get('age')> 7 && kid.get('age') < 12) {
              tP += 1;
            }
          });
        });
        this.set('signedIn', true)
        this.set('totalAdults', tA)
        this.set('totalBabies', tB)
        this.set('totalKids', tK)
        this.set('totalTeens', tT)
        this.set('totalPreTeen', tP)
     } else {
       this.set('signedIn', false)
       this.set('signInErr', true)
     }
   }
  }
});
