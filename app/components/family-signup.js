import Component from '@ember/component';
import { computed, observer  } from '@ember/object';
import { A } from '@ember/array';
import DS from 'ember-data';

export default Component.extend( {
  errors: DS.Errors.create(),
  validate: function() {
    this.set('errors', DS.Errors.create());
    if (this.get('model.lastName') === '' || this.get('model.lastName') === undefined) {
      this.get('errors').add('lastName', "required")
    }
    if (this.get('model.staying') === '' || this.get('model.staying') === undefined) {
      this.get('errors').add('staying', "required")
    }
    if (this.get('model.numAdults') === '' || this.get('model.numAdults') === undefined) {
      this.get('errors').add('numAdults', "required")
    }
    if (this.get('model.numberBabies') === '' || this.get('model.numberBabies') === undefined) {
      this.get('errors').add('numberBabies', "required")
    }
    if (this.get('model.numberTeens') === '' || this.get('model.numberTeens') === undefined) {
      this.get('errors').add('numberTeens', "required")
    }
    if (this.get('model.numberChildren') === '' || this.get('model.numberChildren') === undefined) {
      this.get('errors').add('numberChildren', "required")
    }
    return this.get('errors.isEmpty')
  },
  validateNames: function() {
    this.set('errors', DS.Errors.create());
    var adults = this.get('model.adults');
    for(var i = 0; i< this.get('model.numAdults'); i++) {
      if(adults[i] === undefined || adults[i] === "") {
          let temp = "adult" + i
          this.get('errors').add(temp, "required")
      }
    }
    var kids = this.get('model.children').toArray();
    for (var k = 0; k<kids.length; k++){
      var kid = kids[k];
      if (kid.get('firstName') === "" || kid.get('firstName') === undefined) {
        let temp = "childName" + k;
        this.get('errors').add(temp, "required")
      }
      if (kid.get('age') === "" || kid.get('age') === undefined) {
        let temp = "childAge" + k;
        this.get('errors').add(temp, "required")
      }
      if (parseInt(kid.get('age')) > 11 && kid.get('shirtSize') === undefined) {
        let temp = "childShirtSize" + k;
        this.get('errors').add(temp, "required")
      }
    }
    return this.get('errors.isEmpty')
  },
  validateAddress: function() {
    this.set('errors', DS.Errors.create());
    if (this.get('contact.name') === '' || this.get('contact.name') === undefined) {
      this.get('errors').add('name', "required")
    }
    if (this.get('contact.email') === '' || this.get('contact.email') === undefined) {
      this.get('errors').add('email', "required")
    }
    if (this.get('contact.cellPhone') === '' || this.get('contact.cellPhone') === undefined) {
      this.get('errors').add('cellPhone', "required")
    }
    if (this.get('address.addressLine1') === '' || this.get('address.addressLine1') === undefined) {
      this.get('errors').add('addressLine1', "required")
    }
    if (this.get('address.city') === '' || this.get('address.city') === undefined) {
      this.get('errors').add('city', "required")
    }
    if (this.get('address.stateProvinceRegion') === '' || this.get('address.stateProvinceRegion') === undefined) {
      this.get('errors').add('stateProvinceRegion', "required")
    }
    if (this.get('address.country') === '' || this.get('address.country') === undefined) {
      this.get('errors').add('country', "required")
    }
    if (this.get('address.zipPostalCode') === '' || this.get('address.zipPostalCode') === undefined) {
      this.get('errors').add('zipPostalCode', "required")
    }
    return this.get('errors.isEmpty')
  },
  totalAdults: '',
  totalKids: '',
  numOfAdults: computed('model.numAdults', function() {
    if (this.get('model.numAdults')) {
       const times = parseInt(this.get('model.numAdults'));
       return new Array(times);
    }
 }),

 numberBabies: observer('model.kids', function() {
  var count = 0;
  for (var kid in this.get('model.kids')) {
    if (kid.age < 2) {
      count++;
    }
  }
  this.set('model.numberBabies', count)
}),

adults: observer('model.numAdults', function() {
  if (this.get('model.numAdults')) {
     const times = parseInt(this.get('model.numAdults'));
     this.set('model.adults', A(new Array(times)))
  }
}),

tshirtSizes: A([
            "XS",
            "S",
            "M",
            "L",
            "XL"
          ]),
ages: A([
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18"
        ]),
actions: {
  //set list of adults names
  setAdult(index, value) {
    var temp = this.get('model.adults')
    temp[index] = value
    var error = "adult" + index;
    this.set('model.adults', temp)
    if (!this.get('errors.isEmpty')) {
      this.get('errors').remove(error)
    }
  },
  //saves model
  click() {
      this.get('model').save().then(function() { })
      this.set('complete', true)
    
  },
  pay() {
    this.set('model.paid', true)
    this.get('model').save().then(function() { })
      this.set('complete', true)
  },
  //goes to name fillout for family
  familyNext() {
    if (this.validate()) {
      var kids = A()
      var total = parseInt(this.get('model.numberBabies')) + parseInt(this.get('model.numberTeens')) + parseInt(this.get('model.numberChildren'));
      for (var k=0; k < total; k++) {
        let temp = this.store.createRecord('child', {})
        kids.pushObject(temp)
      }
      this.set('model.children', kids)
      this.set('nextButton', "info")
    }
  },
  //goes to info fillout
  infoNext() {
    if (this.validateNames()) {
      var costTwenty = parseInt(this.get('model.numAdults')) + parseInt(this.get('model.numberTeens'));
      var costTwelve = parseInt(this.get('model.numberChildren'));
      var cost = (20 * costTwenty) + (12 * costTwelve);
      if (cost > 125) {
        cost = 125;
      }
      this.set('model.cost', cost)
      this.set('nextButton', "payment")
    }
  },
  //get to payment
  payNext() {
    if(this.validate()) {
      this.set('nextButton', "save")
    }
  },
  //sets button back
  back() {
    if (this.get('nextButton') === "info") {
      this.set('nextButton', "family")
    } else if(this.get('nextButton') === "payment") {
      this.set('nextButton', "info")
    } else if(this.get('nextButton') === "save") {
      this.set('nextButton', "payment")
    }
  },
  //set button back to info

  update(error, val, data) {
    if (!this.get('errors.isEmpty')) {
      this.get('errors').remove(error)
    }
    this.set(val, data)
  },
  selectShirtSize(child, index, value) {
    var error = "childShirtSize" + index;
    if (!this.get('errors.isEmpty')) {
      this.get('errors').remove(error)
    }
    child.set('shirtSize', value.target.value);
  },
  selectChildName(error, child, data) {
    if (!this.get('errors.isEmpty')) {
      this.get('errors').remove(error)
    }
    child.set('firstName', data);
  },
  selectChildAge(error, child, data) {
    if (!this.get('errors.isEmpty')) {
      this.get('errors').remove(error)
    }
    child.set('age', data.target.value);
  }
}

});
