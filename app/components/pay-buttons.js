import Component from '@ember/component';

export default Component.extend({
  actions: {

  pay() {
    this.set('model.paid', true)
    this.get('model').save().then(function() { })
      this.set('complete', true)
  },
  save() {
    this.get('model').save().then(function() { })
    this.set('complete', true)
  }
}
});
