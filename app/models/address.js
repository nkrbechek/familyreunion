import DS from 'ember-data';

export default DS.Model.extend({
      addressLine1: DS.attr('string'),
      addressLine2: DS.attr('string'),
      city: DS.attr('string'),
      stateProvinceRegion: DS.attr('string'),
      zipPostalCode: DS.attr('string'),
      country: DS.attr('string')
});
