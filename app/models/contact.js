import DS from 'ember-data';

export default DS.Model.extend({
  cellPhone: DS.attr('string'),
  homePhone: DS.attr('string'),
  email: DS.attr('string'),
  address: DS.belongsTo('address',{ async: true}),
  name: DS.attr('string')
});
