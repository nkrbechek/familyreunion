import DS from 'ember-data';

export default DS.Model.extend({
    firstName: DS.attr('string'),
    age: DS.attr('number'),
    shirtSize: DS.attr('string') //should be nil if 12<age<18
});
