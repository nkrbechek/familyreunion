import DS from 'ember-data';

export default DS.Model.extend({
    firstName: DS.attr('string'),
    lastName: DS.attr('string'),
    relative: DS.attr('string'),
    contact: DS.belongsTo('contact', {async: true}), // name should be set to individual firstname
    staying: DS.attr('string'),
    cost: DS.attr('string', {defaultValue: "20"}), // set to 20 by default of one adult
    paid: DS.attr('boolean', {defaultValue: false}),
});
