import DS from 'ember-data';

export default DS.Model.extend({
    adults: DS.attr(), // should be an array of strings for the firstnames of the adults
    children: DS.hasMany('child', { async: true}),
    relative: DS.attr('string'),
    numberBabies: DS.attr('number', {defalutValue: 0}), //computed - ages 0-2
    numberChildren: DS.attr('number', {defalutValue: 0}), //computed - ages 3-11
    numberTeens: DS.attr('number', {defalutValue: 0}), //computed - ages 12-18
    lastName: DS.attr('string'),
    staying: DS.attr('string'),
    numAdults: DS.attr('number', {defalutValue: 0}),
    contact: DS.belongsTo('contact',{ async: true}),
    cost: DS.attr('string'), // computed - $20 ages > 12, $12 ages 3-11. MAX $125
    paid: DS.attr('boolean', {defaultValue: false}),
});
